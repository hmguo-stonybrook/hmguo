%%Gaussian (Gauss-Jordan) elimination
%%d
function x = gausd(A,b)
m=length(A(:,1));%The number of rows of matrix
n=length(A(1,:));%The number of columns of matrix
if m~=length(b)
    error('The number of rows of matrix should be equal to the length of vector')
end

Aug=[A,b];        %get Augmented matrix
[E]=gausb(Aug);    %use function in b(gausb) to convert augmented matrix in echelon form E
[C]=gausc(E);   %use function in c(gausc) to convert E in the row canonical form C

if rank(C)~=min(m,n)
    error('No possible solution!')
end
for i=1:n
    x(i,1)=C(i,size(C,2));
end
end
    