%%Q1 
clear all;
N = 10000;  %Set a maximum number of random points
for i=10:N; %get a fixed number of random points
    a1=rand(i,1); 
    b1=rand(i,1); %generate two sequences of random number between 0 to 1
    distant = a1.^2+b1.^2<1; %count the number of points that the distant between the point and the orign less than 1 
    x(i)=i;%give number of all points i to x
    y1(i)=4*sum(distant)/i; %caculate the simulation of Pi
end
y2=y1-pi;%caculate the difference between true Pi and the simulation of Pi
subplot(2,1,1);
plot(x,y1,x,y2)%%plot difference and simulation Pi with number of points
title('The value of Pi and the difference from the true Pi as the number of random points N');
xlim([10 10000]);
legend('Pi value from Monte Carlo','difference from true value of Pi')


clear all; % compare the computational time cost with precision
for i=1:8; % set the level of the number of the whole points
    tic; %tic toc begin
    a2=rand(10^i,1);
    b2=rand(10^i,1);
    distantpre = a2.^2+b2.^2<1;
    n(i)=i;
    y3(i)=4*sum(distantpre)/10^i;
    t(i)=toc; %tic toc end
end
subplot(2,1,2);
plot(n,t) %plot the computational time cost with precision
title('The computational time cost with precision');

%%%%%Q2
clear all;
pre=3;%set precise level��3 significant figures
steps=9; prelevel=10^(-pre); % begin ��from N=10
while 1
    steps=steps+1; 
    a0= rand(steps,1);
    b0= rand(steps,1);
    y0(steps) = 4*sum(a0.^2+b0.^2<1)/steps; %caculate the pi of N=steps
    a1= rand(steps+1,1);
    b1= rand(steps+1,1);
    y1(steps) = 4*sum(a1.^2+b1.^2<1)/(steps+1);%caculate the pi of N=steps+1
    if abs(y0(steps)-y1(steps))<=prelevel break;end %stop the loop when diff of two values of pi less than precise level
end
significantfigures=pre;%significant figures
simpi=y0(steps);%the value of pi which is fixed level of precision
steps; %how many steps


%%%%%Q3

function x=compupi(pre)%input the level of precision
steps=9; prelevel=10^(-pre); % begin ��from N=10
while 1
    steps=steps+1; 
    a0= rand(steps,1);
    b0= rand(steps,1);%generate two sequences of random number between 0 to 1
    y0(steps) = 4*sum(a0.^2+b0.^2<1)/steps; %caculate the pi of N=steps
    a1= rand(steps+1,1);
    b1= rand(steps+1,1);
    y1(steps) = 4*sum(a1.^2+b1.^2<1)/(steps+1);%caculate the pi of N=steps+1
    if abs(y0(steps)-y1(steps))<=prelevel break;end %stop the loop when diff of two values of pi less than precise level
end
distan=sqrt(a0.^2+b0.^2);%get all distants from points to origin
k=1;g=1;
for i=1:steps
    if distan(i)<=1
        ak(k)=a0(i);
        bk(k)=b0(i);%get all points in circle be ak and bk
        k=k+1;
    else
        ag(g)=a0(i);
        bg(g)=b0(i);%get all points outside of circle be ag and bg
        g=g+1;
    end
end
plot(ak,bk,'r.',ag,bg,'.');%plot two kinds of points
str=['PI=',num2str(y0(steps))];
text(0.71,0.71,str,'FontSize',26);%text value of pi on the figure
xlabel('x'); ylabel('y');
x=y0(steps);
end
    