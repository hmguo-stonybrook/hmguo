%%Matrix inversion and LU decomposition
%%a-forward
function [A,P]=decomaf(A)
[m,n]=size(A);     %use m,n to save the size of matrix
a=1;               %a will is the column number that we begin to find
P=diag(linspace(1,1,m))  ;   %set P is a diag matrix
for i=1:m
    if i==m          %if row i is the last non-zero row, the matrix is now in an echelon form
        break;
    end
    for j=a:n
        if A(i,j)==0
           for k=i+1:m
               if A(k,j)~=0  %if we find any entry below or left of A(i,j) is not 0, which means the matrix is not a echelon form.
                   p1=diag(linspace(1,1,m));   %define row-changing matrix for changing the row of i and k 
                   p1(i,i)=0;p1(k,k)=0;p1(i,k)=1;p1(k,i)=1;
                   A=p1*A;          %new A ; after chaging i and k
                   P=p1*P;    %%save p1 to P
                   break;
               else
                   break;
               end
           end
        end 
        for k=i+1:m          % This part is using pivot to zero out the corresponding entry in all rows below it
           if A(k,j)~=0
           p2=diag(linspace(1,1,m));
           p2(k,i)=-A(k,j)/A(i,j);
           A=p2*A;    %%new A; after zero out the entries above the pivot
           P=p2*P;    %%save p2 to P
           end
        end
        if A(i,j)~=0   %if B(i,j)is nonzero, it should be the pivot(of this row), then we can move to next row to find pivots
        break;         %use 'break' to move to next row.
        end
    end
    a=a+j;    
end
end
