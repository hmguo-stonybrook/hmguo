%%Matrix inversion and LU decomposition
%b
function [I]=decomb(A)       %input is a matrix A  
if abs(det(A)<=(1.0e-15))    %if A is singular, give an error
    error('The matrix is SINGULAR!')
end
[m,n]=size(A);               %get size of A
aug1=[A,diag(linspace(1,1,m))]; %get augmented matrix of A and I
[aug,~]=decomaf(aug1);          %do forward elimination 
[inverse,~]=decomab1(aug,m,n);  %do back-substitution
I=inverse(1:n,m+1:2*m)  % we only need half right of the output

end
%%Matrix inversion and LU decomposition
%a-back
function [A,Q]=decomab1(A,m,n)        %%%%we need to set m and n   
% I copy the most of code from gausa, it will return an error if the matrix is NOT in echelon form.
eche=0;          %eche is the indicator of matrix 
for i=1:m        %first for loop. 
    for j=1:n    %second for loop
        if A(i,j)~=0  
            if i == m
              x=i;
              y=j-1;
            else
              x=i+1;
              y=j;
            end   
            for k=x:m
                for l=1:y
                    if A(k,l)~=0
                    eche=1;  %if we find any entry below or left of A(i,j) is not 0, which means the matrix is not a echelon form.
                             %We set indecator is 1
                    end
                end
            end
            break;     %use 'break' to break second 'for' if we find pivot 
        end
    end
end
for i=m:-1:1    %check if there is a all zero row above the non zero row, since it's not a echelon form
    for j=1:n    
        if A(i,j)~=0
            break;
        end
    end
    if A(i,j)~=0
        break;
    end
end
for k=1:i
    if A(k,1:n)==0   
        eche=1;
    end
end
if eche           
    error('The matrix is NOT in echelon form!')
end

%%%now the back part

Q=diag(linspace(1,1,n));%set Q is a diag matrix
for i=n:-1:1       %Begin from the last non-zero row
    if i==1   %if i is 1,  the loop has been first row, so break
                break;
    end
    for j=1:m      %Begin from the first non-zero entry in row i
        if A(i,j)~=0   %Convert the pivot entry to 1 
            q1=diag(linspace(1,1,n));           %define changing matrix for changing the row value 
            q1(i,i)=(1/A(i,j));
            A=q1*A;
            Q=q1*Q;                %add q1 to Q
            
            for k=1:i-1
                if  A(k,j)~=0    
                    q2=diag(linspace(1,1,n));%Use the pivot to zero out the corresponding entry in all rows above it
                    q2(k,i)=-A(k,j)/A(i,j);  %
                    A=q2*A;
                    Q=q2*Q;         %add q2 to Q
                end
            end
            break;
        end
    end
end
Q
disp('The canonical form of input matrix:')
end                
            
          
            