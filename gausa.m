%%Gaussian (Gauss-Jordan) elimination
%%a
function gausa(A)    % input A is a matrix
[m,n]=size(A);     % A is a matrix that m*n
eche=0;          %eche is the indicator of matrix 
a=0;             %set a = 0
for i=1:m        %first for loop
    for j=1:n    %second for loop
        if A(i,j)~=0  
            if i == m
              x=i;
              y=j-1;
            else
              x=i+1;
              y=j;
            end   
            for k=x:m
                for l=1:y
                    if A(k,l)~=0
                    eche=1;  %if we find any entry below or left of A(i,j) is not 0, which means the matrix is not a echelon form.
                             %We set indecator is 1
                    end
                end
            end
            a=a+1;
            X(1,a)=i;  %use X, Y to save the pivot for follow part of function
            Y(1,a)=j;
            break;     %use 'break' to break second 'for' if we find pivot 
        end
    end
end
for i=m:-1:1    %check if there is a all zero row above the non zero row, since it's not a echelon form
    for j=1:n    
        if A(i,j)~=0
            break;
        end
    end
    if A(i,j)~=0
        break;
    end
end
for k=1:i
    if A(k,1:n)==0   
        eche=1;
    end
end
if eche           
    disp('The matrix is NOT an echelon form')     %output the result
else
    cano=0;    %cano is a indicator of A to check if it is a row canonical form
    for i=1:a
        if A(X(1,i),Y(1,i))~=1   %check if the pivot is 1 
            cano=1;   %if pivot is not 1, cano=1, which means A is not a row canonical form
        else
            if X(1,i)>1
                for k=1:X(1,i)-1
                    if A(k,Y(1,i))~=0 %a row canonical form matrix must have all zero above the 1
                        cano=1;    %if not, cano=1
                    end
                end
            end
        end
    end
    if cano
        disp('The matrix is an echelon form')
    else
        disp('The matrix is a row canonical form')
    end
end
end
