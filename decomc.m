
function [L,A,P]=decomc(A)
[m,n]=size(A);     %use m to save the number of row of the input matrix
c=1;             %c will determine that on which column we start to fi
if m~=n
    error('The matrix should be square!')
end
P=diag(linspace(1,1,n));   %%set two matrix for following using
L=diag(linspace(1,1,n));
for i=1:n
    for j=c:m
        if A(i,j)==0
           for k=i+1:n
               if A(k,j)~=0  %find pivots
                   p1=diag(linspace(1,1,n));   %set a row-changing matrix p1 
                   p1(i,i)=0;p1(k,k)=0;p1(i,k)=1;p1(k,i)=1;  %row exchange
                   A=p1*A;                     %row exchange on A
                   P=P*p1;       %compute P by multip p1
                   break;
               else
                   break;
               end
           end
        end 
        for k=i+1:n
           if A(k,j)~=0
           q1=diag(linspace(1,1,n)); %Use the pivot to zero out the corresponding entry in all rows below it
           q1(k,i)=-A(k,j)/A(i,j);
           A=q1*A;
           L=L/inv(q1);              %compute L
           end
        end
        if A(i,j)~=0   
            break;         
        end
    end
    c=c+j;
    if i==n          %if ??is the last non-zero row, the matrix is now in an echelon form, so end the loop
        break;
    end
end
end

                
            
          
            