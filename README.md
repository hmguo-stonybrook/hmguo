# ABOUT ME #

This document contains some information about me.

### The Basic Information ###

* Full Name: Haiming Guo
* Stony Brook ID: 111120732
* E-mail: Haiming.Guo@stonybrook.edu
* Bitbucket ID: Haiming Guo

### A Brief Introduction ###

* I come from China, and my graduated from USTC(University of Science and Technology of China). 
* My major was mathematics and applied mathematics in USTC, and my track is statistics in AMS.