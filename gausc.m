%%Gaussian (Gauss-Jordan) elimination
%%c
function [A]=gausc(A)% input A is an echelon form matrix



%%%now the row canonical form part

for i=m:-1:1     %Begin from the last non-zero row
    for j=1:n    %Begin from the first non-zero entry in row i
        if A(i,j)~=0   %Convert the pivot entry to 1 by multiplying A(i,:) by (1/A(i,j))
            A(i,:)= A(i,:).*(1/A(i,j));
            if i==1   %if is row 1, the matrix is now in row-canonical form, so we break the loop
                break;
            end
            for k=1:i-1
                if  A(k,j)~=0    
                    A(k,:)=A(k,:)-A(k,j).*A(i,:);%Use the pivot to zero out the corresponding entry in all rows above it
                end
            end
            break;
        end
    end
end
end