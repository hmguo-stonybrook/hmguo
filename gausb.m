%%Gaussian (Gauss-Jordan) elimination
%%b
function [A]=gausb(A)% input A is a matrix
m=length(A(:,1));     % 
n=length(A(1,:));     % A is a matrix that m*n
a=1;             %a will is the column number that we begin to find
for i=1:m
    if i==m    %if row i is the last non-zero row, the matrix is now in an echelon form
        break;
    end
    for j=a:n
        if A(i,j)==0
           for k=i+1:m
               if A(k,j)~=0  %find row k that A(k,j) is not zero
                   t=A(i,:); %change row i with row k
                   A(i,:)=A(k,:);
                   A(k,:)=t;
                   break;
               else
                   break;
               end
           end
        end 
        for k=i+1:m
           if A(k,j)~=0 
           A(k,:)=A(k,:)-(A(k,j)/A(i,j))*A(i,:);   %use the pivot to zero out the corresponding entry in all rows below it
           end
        end
        if A(i,j)~=0   %check that if we have found the pivot, we let i+1 to next row; or j+1 to next column
        break;         
        end
    end
    a=a+j;
end
end